var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');

var mysql = require('mysql');
var dbConn = mysql.createConnection({
    host: '35.240.175.85',
    user: 'root',
    password: 'E*q47cua',
    database: 'Test_db'
});

// connect to database
dbConn.connect();

app.use(cors({origin:true}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// default route
app.get('/', function (req, res) {
    return res.send({ error: true, message: 'hello' })
});

// Retrieve all users 
app.get('/employee', function (req, res) {
    dbConn.query('SELECT * FROM employee', function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'users list.' });
    });
});

// Retrieve user with id 
app.get('/employee/:id', function (req, res) {

    let emp_id = req.params.id;

    if (!emp_id) {
        return res.status(400).send({ error: true, message: 'Please provide emp_id' });
    }

    dbConn.query('SELECT * FROM employee where id=?', emp_id, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'employees list.' });
    });

});


// Add a new user  
app.post('/employee', function (req, res) {
    console.log(req.body)
    let emp = req.body;
    // console.log(emp)
    if (!emp) {
        return res.status(400).send({ error: true, message: 'Please provide user' });
    }

    dbConn.query("INSERT INTO employee SET ? ", emp, function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'New user has been created successfully.' });
    });
});

//  Update user with id
app.put('/employee', function (req, res) {

    let emp_id = req.body.id;
    let emp = req.body;
    console.log(emp)
    if (!emp_id || !emp) {
        return res.status(400).send({ error: user, message: 'Please provide user and user_id' });
    }

    dbConn.query("UPDATE employee SET firstname = ? WHERE id = ?", [emp.firstname, emp_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'user has been updated successfully.' });
    });
});

//  Delete user
app.delete('/employee', function (req, res) {
    console.log(req.body)
    let emp_id = req.body.id;

    if (!emp_id) {
        return res.status(400).send({ error: true, message: 'Please provide user_id' });
    }
    dbConn.query('DELETE FROM employee WHERE id = ?', [emp_id], function (error, results, fields) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'User has been updated successfully.' });
    });
});

// set port
app.listen(4000, function () {
    console.log('Node app is running on port 4000');
});
module.exports = app;